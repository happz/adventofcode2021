#!/usr/bin/env python

import itertools
import sys
from typing import Iterable, Generator, Tuple


def skip_first(seq: Iterable[str], skip_first: bool) -> Generator[str, None, None]:
    _ = next(seq) if skip_first else None

    yield from seq


def main() -> None:
    counter: int = 0

    i1, i2 = itertools.tee(sys.stdin)

    for current, previous in zip(skip_first(i1, True), skip_first(i2, False)):
        if int(current.strip()) > int(previous.strip()):
            counter += 1

    print(counter)


if __name__ == '__main__':
    main()
