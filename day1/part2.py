#!/usr/bin/env python

import itertools
import sys
from typing import Iterable, Generator, Tuple


def iter_windows(seq: Iterable[str], skip_first: bool) -> Generator[Tuple[str, str, str], None, None]:
    window_size = 3

    # skip first depth record
    _ = next(seq) if skip_first else None

    # Initialize the window - it will have 3 items, and we need it first so we could drop and add items to it
    # in the loop below.
    window = (next(seq), next(seq), next(seq))

    yield window

    for i in seq:
        window = window[1:] + (i,)

        yield window


def window_sum(window: Tuple[str, str, str]) -> int:
    return sum([int(s.strip()) for s in window])


def main() -> None:
    counter: int = 0

    i1, i2 = itertools.tee(sys.stdin)

    for current_window, prev_window in zip(iter_windows(i1, True), iter_windows(i2, False)):
        if window_sum(current_window) > window_sum(prev_window):
            counter += 1

    print(counter)


if __name__ == '__main__':
    main()
