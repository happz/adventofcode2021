#!/usr/bin/env python

import itertools
import sys
from typing import Iterable, Generator, Tuple


def main() -> None:
    x, y, aim = 0, 0, 0

    for line in sys.stdin:
        cmd, value = line.strip().split(' ')

        if cmd == 'forward':
            x += int(value)
            y -= (int(value) * aim)

        elif cmd == 'up':
            aim -= int(value)

        elif cmd == 'down':
            aim += int(value)

    print(x * -y)


if __name__ == '__main__':
    main()
