#!/usr/bin/env python

import itertools
import sys
from typing import Iterable, Generator, Tuple


def main() -> None:
    x, y = 0, 0

    for line in sys.stdin:
        cmd, value = line.strip().split(' ')

        if cmd == 'forward':
            x += int(value)

        elif cmd == 'up':
            y += int(value)

        elif cmd == 'down':
            y -= int(value)

    print(x * -y)


if __name__ == '__main__':
    main()
