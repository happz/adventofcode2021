DAYS := $(sort $(shell find . -name 'day*' -type d))

.PHONY: $(DAYS)

all: $(DAYS)

venv:
	poetry install

day1: venv
	poetry run $@/part1.py < $@/part1.txt
	poetry run $@/part2.py < $@/part2.txt

day2: venv
	poetry run $@/part1.py < $@/part1.txt
	poetry run $@/part2.py < $@/part2.txt

day3: venv
	poetry run $@/part1.py < $@/part1.txt
	poetry run $@/part2.py < $@/part2.txt
