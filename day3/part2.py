#!/usr/bin/env python

import itertools
import sys
from typing import Iterable, Generator, Tuple


Number = str


def iter_numbers() -> Generator[Number, None, None]:
    for line in sys.stdin:
        yield line.strip()


def iter_nth_bit(numbers: Iterable[Number], offset: int) -> Generator[int, None, None]:
    for number in numbers:
        yield int(number[offset])


def to_countable(i: int) -> int:
    return -1 if i == 0 else i


def most_common(bits: Iterable[int]) -> int:
    return 1 if sum(to_countable(bit) for bit in bits) >= 0 else 0


def least_common(bits: Iterable[int]) -> int:
    return 1 if sum(to_countable(bit) for bit in bits) < 0 else 0


def match_bit(number: Number, offset: int, bit: int) -> bool:
    return number[offset] == str(bit)


def iter_matching_nth_bit(numbers: Iterable[Number], offset: int, bit: int) -> Generator[Number, None, None]:
    for number in numbers:
        if not match_bit(number, offset, bit):
            continue

        yield number


def reduce_numbers(numbers: Iterable[Number], bit_picker) -> Number:
    ENTRY_SIZE = 12

    reduced_set = list(numbers)

    for offset in range(0, ENTRY_SIZE):
        if len(reduced_set) == 1:
            return reduced_set[0]

        reduced_set = list(
            iter_matching_nth_bit(reduced_set, offset, bit_picker(iter_nth_bit(reduced_set, offset)))
        )

    if len(reduced_set) == 1:
        return reduced_set[0]

    assert False, 'reduced to nothing...'


def main() -> None:
    i1, i2 = itertools.tee(iter_numbers())

    oxygen = int(reduce_numbers(i1, most_common), base=2)
    co2 = int(reduce_numbers(i2, least_common), base=2)

    print(oxygen * co2)


if __name__ == '__main__':
    main()
