#!/usr/bin/env python

import itertools
import sys
from typing import Iterable, Generator, Tuple


def iter_nth_bit(lines: Iterable[str], offset: int) -> Generator[int, None, None]:
    for line in lines:
        yield int(line.strip()[offset])


def to_countable(i: int) -> int:
    return -1 if i == 0 else i


def main() -> None:
    ENTRY_SIZE = 12

    gamma, epsilon = 0, 0

    iterators = list(itertools.tee(sys.stdin, ENTRY_SIZE * 2))

    for offset in range(0, ENTRY_SIZE):
        most_common_sum = sum(
            to_countable(bit)
            for bit in iter_nth_bit(iterators.pop(0), offset)
        )

        least_common_sum = sum(
            to_countable(bit)
            for bit in iter_nth_bit(iterators.pop(0), offset)
        )

        most_common_bit = 1 if most_common_sum > 0 else 0
        least_common_bit = 0 if least_common_sum > 0 else 1

        gamma |= most_common_bit << (ENTRY_SIZE - offset - 1)
        epsilon |= least_common_bit << (ENTRY_SIZE - offset - 1)

    print(gamma * epsilon)


if __name__ == '__main__':
    main()
